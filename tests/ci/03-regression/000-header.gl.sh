rm -rf /tmp/.mpb
mkdir -p /tmp/.mpb

LOG_FILE="/tmp/.mpb/ci_test.log"
CONFIG_FILE="/tmp/.mpb/ci_mpb.config"
EXCEPTION_LOG="${HOME}/.mpb/mpb.log"

function FAIL() {
  echo -e "${BRed}Test FAILED${Color_Off}"
  if [ -f ${LOG_FILE} ]
  then
    cat ${LOG_FILE}
    res=$(grep "Exception log stored" ${LOG_FILE})

    if [ ! "x${res}" == "x" ]
    then
      cat ${EXCEPTION_LOG}
    fi
  fi
  false
}

function XFAIL() {
  if [ $? -eq 0 ]
  then
    FAIL
  fi
  true
}

${SUDO} dnf install -y \
  koji \
  copr-cli \
  python3-pyyaml \
  wget \
  tree \
  diffutils

DATE=$(date)
echo -e "${Purple}Starting date: ${DATE}${Color_Off}"
