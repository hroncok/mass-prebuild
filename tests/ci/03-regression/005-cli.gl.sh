echo -e "${BBlue}##### Checking main CLI #####${Color_Off}"

DATE=$(date)
echo -e "${Purple}Starting date: ${DATE}${Color_Off}"

rm -rf ${EXCEPTION_LOG}

echo -e "${Blue}Empty config test${Color_Off}"
cat > ${CONFIG_FILE} << EOF
verbose: 5
EOF

mpb --config=${CONFIG_FILE} > ${LOG_FILE} && XFAIL
res=$(grep "Configuration is broken" ${LOG_FILE} || true)

if [ "x${res}" == "x" ]
then
  FAIL
fi

echo -e "${Blue}Non-existent build ID test${Color_Off}"
cat > ${CONFIG_FILE} << EOF
build_id: 999999999
clean: True
EOF

mpb --config=${CONFIG_FILE} > ${LOG_FILE} && XFAIL
res=$(grep "Build ID doesn't match any known ID" ${LOG_FILE} || true)

if [ "x${res}" == "x" ]
then
  FAIL
fi

echo -e "${Blue}Invalid arch${Color_Off}"
cat > ${CONFIG_FILE} << EOF
archs: blabla
packages: autoconf
revdeps:
  list: automake
backend: dummy
verbose: 5
EOF

mpb --config=${CONFIG_FILE} > ${LOG_FILE} && XFAIL
res=$(grep "is not a supported architecture" ${LOG_FILE} || true)

if [ "x${res}" == "x" ]
then
  FAIL
fi

echo -e "${Blue}Invalid back-end${Color_Off}"
cat > ${CONFIG_FILE} << EOF
archs: x86_64
packages: autoconf
revdeps:
  list: automake
verbose: 5
backend: blabla
EOF

mpb --config=${CONFIG_FILE} > ${LOG_FILE} && XFAIL
res=$(grep "Unknown back-end" ${LOG_FILE} || true)

if [ "x${res}" == "x" ]
then
  FAIL
fi

echo -e "${Blue}Dummy back-end (w/o rev deps calculation)${Color_Off}"
cat > ${CONFIG_FILE} << EOF
archs: x86_64
packages: autoconf
revdeps:
  list:
    automake:
      committish: '@last_build'
retry: 3
verbose: 5
backend: dummy
EOF

# May fail on stage 3, but that doesn't matter at this point
mpb --config=${CONFIG_FILE} > ${LOG_FILE} || true
res=$(grep "backend: dummy" ${LOG_FILE} || true)
res1=$(grep "Executing stage 4" ${LOG_FILE} || true)

ID=$(grep -m 1 "mpb --buildid"  ${LOG_FILE} | sed "s/\"//g" | cut -d " " -f 3 || true)

if [ "x${res}" == "x" ] || [ "x${res1}" == "x" ]
then
  FAIL
fi

echo -e "${Blue}Re-use backend information from database${Color_Off}"
mpb --buildid ${ID} -V > ${LOG_FILE} || true
res=$(grep "Using dummy back-end" ${LOG_FILE} || true)

if [ "x${res}" == "x" ]
then
  FAIL
fi

echo -e "${Blue}Cancel existing${Color_Off}"
cat > ${CONFIG_FILE} << EOF
build_id: ${ID}
cancel: True
backend: dummy
EOF

mpb --config=${CONFIG_FILE} > ${LOG_FILE} || FAIL
res=$(grep "Canceling ${ID}" ${LOG_FILE} || true)

if [ "x${res}" == "x" ]
then
  FAIL
fi

echo -e "${Blue}Clean existing${Color_Off}"
cat > ${CONFIG_FILE} << EOF
build_id: ${ID}
clean: True
backend: dummy
EOF

mpb --config=${CONFIG_FILE} > ${LOG_FILE} || FAIL
res=$(grep "Cleaning build ${ID}" ${LOG_FILE} || true)

if [ "x${res}" == "x" ]
then
  FAIL
fi

echo -e "${Blue}Dummy back-end (w/ rev deps calculation)${Color_Off}"
cat > ${CONFIG_FILE} << EOF
archs: x86_64
packages:
  autoconf-archive:
    committish: '@last_build'
retry: 3
verbose: 5
backend: dummy
EOF

res=""
# There is some probability that the main package fails for testing purpose
for i in {0..10}
do
  mpb --config=${CONFIG_FILE} > ${LOG_FILE} || true
  res=$(grep "Calculating reverse dependencies" ${LOG_FILE} || true)
  res1=$(grep "backend: dummy" ${LOG_FILE} || true)
  res2=$(grep "Executing stage 4" ${LOG_FILE} || true)

  if [ "x${res1}" == "x" ] || [ "x${res2}" == "x" ]
  then
    # General failure case
    FAIL
  fi

  if [ ! "x${res}" == "x" ]
  then
    break
  fi
done

if [ "x${res}" == "x" ]
then
  FAIL
fi

echo -e "${Blue}Dummy back-end (w/ group)${Color_Off}"
cat > ${CONFIG_FILE} << EOF
archs: x86_64
packages:
  '@3D Printing':
    committish: '@last_build'
retry: 3
verbose: 5
backend: dummy
EOF

res=""
# There is some probability that the main package fails for testing purpose
for i in {0..20}
do
  mpb --config=${CONFIG_FILE} > ${LOG_FILE} || true
  res=$(grep "Calculating reverse dependencies" ${LOG_FILE} || true)
  res1=$(grep "backend: dummy" ${LOG_FILE} || true)
  res2=$(grep "Executing stage 4" ${LOG_FILE} || true)

  if [ "x${res1}" == "x" ] || [ "x${res2}" == "x" ]
  then
    # General failure case
    FAIL
  fi

  if [ ! "x${res}" == "x" ]
  then
    break
  fi
done

if [ "x${res}" == "x" ]
then
  FAIL
fi

echo -e "${Blue}Dummy back-end (w/ rev deps calculation, append group)${Color_Off}"
cat > ${CONFIG_FILE} << EOF
archs: x86_64
packages: redhat-rpm-config
revdeps:
  append:
    '@3D Printing':
      committish: '@last_build'
retry: 3
verbose: 5
backend: dummy
EOF

res=""
# There is some probability that the main package fails for testing purpose
for i in {0..10}
do
  mpb --config=${CONFIG_FILE} > ${LOG_FILE} || true
  res=$(grep "Calculating reverse dependencies" ${LOG_FILE} || true)
  res1=$(grep "backend: dummy" ${LOG_FILE} || true)
  res2=$(grep "Executing stage 4" ${LOG_FILE} || true)

  if [ "x${res1}" == "x" ] || [ "x${res2}" == "x" ]
  then
    # General failure case
    FAIL
  fi

  if [ ! "x${res}" == "x" ]
  then
    break
  fi
done

if [ "x${res}" == "x" ]
then
  FAIL
fi

echo -e "${Blue}Dummy back-end (w/ rev deps calculation, deps-only packages)${Color_Off}"
# The dummy test verifies that the dependency list contains deps from
# autocon-archive, copr tests will check if this package is actually NOT built
cat > ${CONFIG_FILE} << EOF
archs: x86_64
packages:
  redhat-rpm-config:
    committish: '@last_build'
  autoconf-archive:
    deps_only: True
retry: 3
verbose: 5
backend: dummy
EOF

res=""
# There is some probability that the main package fails for testing purpose
for i in {0..10}
do
  mpb --config=${CONFIG_FILE} > ${LOG_FILE} || true
  res=$(grep "axel" ${LOG_FILE} || true)
  res1=$(grep "backend: dummy" ${LOG_FILE} || true)
  res2=$(grep "Executing stage 4" ${LOG_FILE} || true)

  if [ "x${res1}" == "x" ] || [ "x${res2}" == "x" ]
  then
    # General failure case
    FAIL
  fi

  if [ ! "x${res}" == "x" ]
  then
    break
  fi
done

if [ "x${res}" == "x" ]
then
  FAIL
fi

rm -f ${LOG_FILE} || true

if [ -f ${EXCEPTION_LOG} ]
then
  if [ ! "$(cat ${EXCEPTION_LOG} | wc -l)" == "0" ]
  then
    echo -e "${BRed}Exception raised during tests${Color_Off}"
    cat ${EXCEPTION_LOG}
    FAIL
  fi
fi

echo -e "${BGreen}OK${Color_Off}"

echo ""
