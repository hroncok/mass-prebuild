echo -e "${BBlue}##### Install python deps #####${Color_Off}"
pip install copr PyYaml setuptools posix_ipc

echo -e "${BBlue}##### Build mass-prebuilder #####${Color_Off}"
python3 setup.py build

echo -e "${BBlue}##### Install mass-prebuilder #####${Color_Off}"
pip install .

${SUDO} mkdir -p /etc/mpb
${SUDO} cp -r examples/*.conf.d /etc/mpb

echo ""
