""" MPB copr editor

    This script is meant to allow user to modify an existing COPR project.
    This can be useful when there are thousands of packages to modify, e.g.
    to enable automatic rebuild once Mass pre-builder is done with the initial
    builds.
"""
import argparse
import logging
from pathlib import Path
import sys
import yaml

from mass_prebuild import VERSION

from mass_prebuild.backend import copr
from mass_prebuild.backend.db import authorized_collect, MpbDb

def handle_exception(exc_type, exc_value, exc_traceback):
    """Log exception into a file to ease debugging"""
    if exc_type == SystemExit:
        return

    if issubclass(exc_type, KeyboardInterrupt):
        sys.__excepthook__(exc_type, exc_value, exc_traceback)
        return

    logger().error("Uncaught exception", exc_info=(exc_type, exc_value, exc_traceback))

    release_database()

    print('Stopping due to Uncaught exception !')
    print(f'Exception log stored in {str(Path(Path.home() / ".mpb" / "mpb-copr.log"))}')


def database(config=None):
    """Setup and return the database"""
    if not hasattr(database, "database"):
        database.database = None

    if config:
        database.database = MpbDb(path=config['database'], verbose=config['verbose'])

    return database.database


def release_database():
    """Release the database locks"""
    if database() is not None:
        database().release()


def logger():
    """Setup and return the logger"""
    if not hasattr(logger, "logger"):
        home_path = Path(Path.home() / '.mpb')
        home_path.mkdir(parents=True, exist_ok=True)

        logging.basicConfig(
            filename=str(Path(home_path / 'mpb-copr.log')),
            filemode='a',
            format='%(asctime)s,%(msecs)d %(name)s %(levelname)s %(message)s',
            datefmt='%H:%M:%S',
            level=logging.ERROR,
        )

        logger.logger = logging.getLogger(__name__)

    return logger.logger


def parse_args():
    """Setup argument parser"""
    authorized_collect_ctrl = {
        f'control-{col}'.upper() for col in authorized_collect
    }
    accepted = sorted(authorized_collect) + sorted(authorized_collect_ctrl)
    accepted = [p.lower() for p in accepted]

    parser = argparse.ArgumentParser()

    parser.add_argument(
        '--verbose',
        '-V',
        action='count',
        help='increase output verbosity, may be provided multiple times',
    )
    parser.add_argument(
        '--version',
        '-v',
        action='version',
        version=f'%(prog)s {VERSION}',
        help='show the version number and exit',
    )
    parser.add_argument(
        '--config',
        '-c',
        default='',
        help='''provide user specific config
                values in command line supersedes values in config file''',
    )
    parser.add_argument('--buildid', '-b', type=int, help='build ID to work on')

    return parser.parse_args()


def _get_config(config_file):
    """Load a config file provided by the user"""
    try:
        with open(config_file, encoding='utf-8') as file:
            conf = {}
            try:
                conf = dict(yaml.safe_load(file))
            except yaml.YAMLError as exc:
                print(exc)
            return conf
    except FileNotFoundError:
        if config_file is not None:
            print(f'Can\'t open config file {config_file}')

    return {}


def get_config(config_file):
    """Load a config file provided by the user"""
    path = Path(config_file)
    if path.is_file():
        print(f'Loading {str(path)}')
        return _get_config(config_file)

    path = Path("mpb.config")
    if path.is_file():
        print(f'Loading {str(path)}')
        return _get_config("mpb.config")

    home_path = Path(Path.home() / '.mpb')
    home_path.mkdir(parents=True, exist_ok=True)

    path = Path(home_path / 'config')
    if path.is_file():
        print(f'Loading {str(path)}')
        return _get_config(str(Path(home_path / 'config')))

    return {}


def populate_config():
    """Get config from file if given and initialize default values"""
    home_path = Path(Path.home() / '.mpb')
    home_path.mkdir(parents=True, exist_ok=True)

    args = parse_args()
    config = get_config(args.config)

    config['build_id'] = args.buildid or config.setdefault('build_id', 0)
    config['verbose'] = args.verbose or config.setdefault('verbose', 0)
    config.setdefault('database', str(Path(home_path / 'share' / 'mpb.db')))
    config.setdefault('copr', {})

    config.setdefault('backend', 'copr')
    config.setdefault('base_build_id', 0)
    config.setdefault('stage', -1)
    config.setdefault('chroot', 'fedora-rawhide')
    config.setdefault('archs', 'x86_64')
    config.setdefault('skip_archs', '')
    config.setdefault('cancel', False)
    config.setdefault('clean', False)
    config.setdefault('cleanup', False)
    config.setdefault('enable_priorities', False)
    config.setdefault('collect', False)
    config.setdefault('data', str(Path(home_path / 'data')))
    config.setdefault('dnf_conf', '')
    config.setdefault('collect_list', 'failed')
    config.setdefault('list', False)
    config.setdefault('info', False)
    config.setdefault('list_packages', False)
    config.setdefault('retry', 0)
    config.setdefault('collect_list_override', False)
    config['validity'] = True

    return config


class MpbCoprEdit(copr.MpbCoprBackend):
    """Mass pre-build COPR back-end

        Implement COPR specific functionalities.
    """

    def _init_copr_config(self, config):
        """Init copr specific configuration"""
        super()._init_copr_config(config)

        config['copr'].setdefault('webhook_rebuild', [])

        if isinstance(config['copr']['webhook_rebuild'], str):
            config['copr']['webhook_rebuild'] = config['copr']['webhook_rebuild'].split()

    def enable_webhook_rebuild(self):
        """Enable webhook_rebuild on a list of packages"""
        if not self.config['copr']['webhook_rebuild']:
            return

        if not self.is_completed():
            print('Refusing to enable webhook rebuild on a non-completed build')
            print('Please let the build go through completion and try again')
            return

        print('WARNING: Enabling webhook rebuild may result in discrepancy')
        print('between package real build state and MPB known state.')

        for pkg in self.packages:
            if not any([
                pkg.name in self.config['copr']['webhook_rebuild'],
                pkg.src_pkg_name in self.config['copr']['webhook_rebuild'],
                'all' in self.config['copr']['webhook_rebuild'],
                ]):
                continue

            if pkg.src_type not in [ 'git', 'distgit' ]:
                continue

            src_type = 'distgit'
            data = {
                "package_name": pkg.src_pkg_name,
                "webhook_rebuild": True,
            }

            if pkg.src_type == 'git':
                src_type = 'scm'
                data['scm_type'] = 'git'

            self.client.package_proxy.edit(
                    self.client.config['username'],
                    self.name,
                    pkg.src_pkg_name,
                    src_type,
                    data
                    )

def main():
    """Main function

    Setup argument parser, initialize default configuration, validates user
    input and execute the mass pre-build build.
    """
    logger()
    sys.excepthook = handle_exception

    config = populate_config()

    if config['build_id'] <= 0:
        print('Build ID must be given')
        return 1

    try:
        backend = MpbCoprEdit(database(config), config)
    except ValueError as exc:
        print(exc)
        release_database()
        return 1

    backend.enable_webhook_rebuild()

    release_database()

    return 0
