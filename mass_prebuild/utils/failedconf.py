""" MPB copr editor

    This script is meant to allow user to modify an existing COPR project.
    This can be useful when there are thousands of packages to modify, e.g.
    to enable automatic rebuild once Mass pre-builder is done with the initial
    builds.
"""
import argparse
import logging
from pathlib import Path
import sys
import yaml

from mass_prebuild import VERSION

from mass_prebuild.backend import copr, dummy
from mass_prebuild.backend.backend import MAIN_PKG, ALL_DEP_PKG
from mass_prebuild.backend.db import authorized_status, MpbDb

def handle_exception(exc_type, exc_value, exc_traceback):
    """Log exception into a file to ease debugging"""
    if exc_type == SystemExit:
        return

    if issubclass(exc_type, KeyboardInterrupt):
        sys.__excepthook__(exc_type, exc_value, exc_traceback)
        return

    logger().error("Uncaught exception", exc_info=(exc_type, exc_value, exc_traceback))

    release_database()

    print('Stopping due to Uncaught exception !')
    print(f'Exception log stored in {str(Path(Path.home() / ".mpb" / "mpb-failedconf.log"))}')


def logger():
    """Setup and return the logger"""
    if not hasattr(logger, "logger"):
        home_path = Path(Path.home() / '.mpb')
        home_path.mkdir(parents=True, exist_ok=True)

        logging.basicConfig(
            filename=str(Path(home_path / 'mpb-failedconf.log')),
            filemode='a',
            format='%(asctime)s,%(msecs)d %(name)s %(levelname)s %(message)s',
            datefmt='%H:%M:%S',
            level=logging.ERROR,
        )

        logger.logger = logging.getLogger(__name__)

    return logger.logger


def database(config=None):
    """Setup and return the database"""
    if not hasattr(database, "database"):
        database.database = None

    if config:
        database.database = MpbDb(path=config['database'], verbose=config['verbose'])

    return database.database


def release_database():
    """Release the database locks"""
    if not hasattr(release_database, 'released'):
        release_database.released = False

    if all([database() is not None, not release_database.released]):
        database().release()
        release_database.released = True


def validate_database(config):
    """Validate the database"""
    if not database(config):
        config['validity'] = False


def validate_build_id(config):
    """Validate build ID"""
    if not config['build_id']:
        config['validity'] = False
        return

    if not database():
        print('No valid database to check for build ID')
        config['validity'] = False
        config['build_id'] = 0
        return

    build = database().build_by_id(config['build_id'])

    if not build:
        print(f'Build ID doesn\'t match any known ID {config["build_id"]}')
        config['build_id'] = 0
        config['validity'] = False


def validate_backend(user_config):
    """Validate the provided backend"""
    if any([not database(), user_config['build_id'] <= 0]):
        user_config['validity'] = False
        return

    # First try to get the backend from the database
    build = database().build_by_id(user_config['build_id'])
    build_config = yaml.safe_load(build['config'])

    backend = 'unknown'

    if 'backend' in build_config:
        backend = build_config['backend']

    if backend == 'unknown':
        backend = user_config['backend']
    else:
        user_config['backend'] = backend

    if backend not in ['copr', 'dummy']:
        print('Unknown back-end in database, assuming COPR')
        user_config['backend'] = 'copr'


def backends():
    """List of available back-ends"""
    return {'dummy': dummy.DummyBackend, 'copr': copr.MpbCoprBackend}


def current_backend(config=None):
    """Global backend accessors"""
    if not hasattr(current_backend, "backend"):
        current_backend.backend = None

    if config:
        current_backend.backend = backends()[config['backend']](database(), config)

    return current_backend.backend


def validate_status(config):
    """Validate requested status input"""
    if all([config['status'].upper() not in authorized_status,
            not config['status'].upper() == 'ALL']):
        config['validity'] = False


def parse_args():
    """Setup argument parser"""
    parser = argparse.ArgumentParser()

    parser.add_argument(
        '--verbose',
        '-V',
        action='count',
        help='increase output verbosity, may be provided multiple times',
    )
    parser.add_argument(
        '--version',
        '-v',
        action='version',
        version=f'%(prog)s {VERSION}',
        help='show the version number and exit',
    )
    parser.add_argument('--buildid', '-b', type=int, help='build ID to work on')
    parser.add_argument(
        '--output',
        '-o',
        default='',
        help='''Where to store the new configuration''',
    )
    parser.add_argument(
        '--status',
        '-s',
        default='all',
        help='''The kind of status to list packages for between failed, unconfirmed, check_needed
        and all''',
    )

    return parser.parse_args()


def populate_config():
    """Get config from file if given and initialize default values"""
    home_path = Path(Path.home() / '.mpb')
    home_path.mkdir(parents=True, exist_ok=True)

    args = parse_args()

    config = {}
    config['build_id'] = args.buildid or config.setdefault('build_id', 0)
    config['verbose'] = args.verbose or config.setdefault('verbose', 0)
    config['status'] = args.status or config.setdefault('status', 'all')
    config['output'] = args.output or config.setdefault('output', '')
    config.setdefault('database', str(Path(home_path / 'share' / 'mpb.db')))

    config.setdefault('backend', 'copr')
    config.setdefault('base_build_id', 0)
    config.setdefault('stage', -1)
    config.setdefault('chroot', 'fedora-rawhide')
    config.setdefault('archs', 'x86_64')
    config.setdefault('skip_archs', '')
    config.setdefault('cancel', False)
    config.setdefault('clean', False)
    config.setdefault('cleanup', False)
    config.setdefault('enable_priorities', False)
    config.setdefault('collect', False)
    config.setdefault('data', str(Path(home_path / 'data')))
    config.setdefault('dnf_conf', '')
    config.setdefault('collect_list', 'failed')
    config.setdefault('list', False)
    config.setdefault('info', False)
    config.setdefault('list_packages', False)
    config.setdefault('retry', 0)
    config.setdefault('collect_list_override', False)
    config['validity'] = True

    return config


def populate_package_list(pkg_type, status_type, verbose):
    """Add packages to the configuration"""
    config = {}

    count = 0
    for pkg in [p for p in current_backend().packages if p.pkg_type & pkg_type]:
        if pkg_type & ALL_DEP_PKG:
            if pkg.build_status not in ['FAILED', 'UNCONFIRMED', 'CHECK_NEEDED']:
                continue

            if status_type not in ['ALL', pkg.build_status]:
                continue

        config[pkg.name] = {
                'src': pkg.src,
                'src_type': pkg.src_type,
                }

        if pkg.committish:
            config[pkg.name]['committish'] = pkg.committish
        if pkg.priority:
            config[pkg.name]['priority'] = pkg.priority
        if pkg.skip_archs:
            config[pkg.name]['skip_archs'] = pkg.skip_archs
        if pkg.retry:
            config[pkg.name]['retry'] = pkg.retry

        if verbose > 3:
            end = '\n'
        else:
            end = '\r'

        count += 1
        print(f'Adding {pkg.name:50}', end = end)

    if pkg_type & ALL_DEP_PKG:
        print(f'Added {count} packages to the list')

    return config

def cleanup(config):
    """Remove unnecessary elements"""
    config.pop('build_id', None)
    config.pop('base_build_id', None)
    config.pop('cancel', None)
    config.pop('clean', None)
    config.pop('cleanup', None)
    config.pop('enable_priorities', None)
    config.pop('collect', None)
    config.pop('list', None)
    config.pop('list_packages', None)
    config.pop('info', None)
    config.pop('collect_list_override', None)
    config.pop('stage', None)
    config.pop('copr', None)

    if not config['verbose']:
        config.pop('verbose', None)

    if not config['skip_archs']:
        config.pop('skip_archs', None)

    if not config['retry']:
        config.pop('retry', None)

    if not config['dnf_conf']:
        config.pop('dnf_conf', None)

    if config['backend'] == 'copr':
        config.pop('backend', None)

    if config['collect_list'] == 'failed':
        config.pop('collect_list', None)

    config.pop('output', None)
    config.pop('status', None)
    config.pop('validity', None)

    if not config['revdeps']['list']:
        config.pop('revdeps', None)


def main():
    """Main function

    Setup argument parser, initialize default configuration, validates user
    input and execute the mass pre-build build.
    """
    logger()
    sys.excepthook = handle_exception

    config = populate_config()

    validate_database(config)
    validate_build_id(config)
    validate_backend(config)
    validate_status(config)

    if not config['validity']:
        print('Invalid parameters')
        release_database()
        return 1

    print('Setting up the back-end')
    try:
        current_backend(config)
    except ValueError as exc:
        print(exc)
        release_database()
        return 1

    status_type = config['status'].upper()
    verbose = config['verbose']

    print('Creating the config')
    new_config = current_backend().config

    new_config['packages'] = {}
    new_config['revdeps'] = {}

    new_config['packages'] = populate_package_list(MAIN_PKG, status_type, verbose)
    new_config['revdeps']['list'] = populate_package_list(ALL_DEP_PKG, status_type, verbose)

    release_database()

    new_config['name'] = f'{current_backend().name}.failed'

    cleanup(new_config)

    path = Path(config['output'])

    if any([not path.parent.exists(), config['output'] == '']):
        path = Path(f'./{new_config["name"]}.config')

    print(f'Storing new configuration in {str(path)}')

    with open(str(path), 'w', encoding='utf-8') as file:
        yaml.dump(new_config, file)

    return 0
