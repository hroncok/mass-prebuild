# pylint: disable = missing-module-docstring
VERSION_INFO = (0, 5, 0)
VERSION = '.'.join(str(c) for c in VERSION_INFO)
